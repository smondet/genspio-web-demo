module Message = struct
  module To_worker = struct
    type t = string

    let to_json : t -> Js.js_string Js.t = Json.output

    let of_json : Js.js_string Js.t -> t = Json.unsafe_input
  end

  module From_worker = struct
    type eval_error =
      [ `Eval_failure of string list
      | `Top_level_exception of exn
      | `Stack_overflow of string list ]

    type r =
      { toplevel_success: string
      ; stdml_compilation: (string * string, eval_error) result
      ; slow_flow_compilation: (string * string, eval_error) result }

    type t =
      | Toplevel_failed of eval_error
      | Toplevel of r
      | Not_ready
    (* type t = [`Done of r | `Not_ready] *)

    let to_json : t -> Js.js_string Js.t = Json.output

    let of_json : Js.js_string Js.t -> t = Json.unsafe_input
  end
end
