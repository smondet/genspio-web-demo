open Nonstd
module String = Sosa.Native_string

let debug f = Printf.ksprintf (fun s -> Firebug.console##log (Js.string s)) f

let global_ocaml =
  let error_stack = ref [] in
  let tmp_count = ref 0 in
  let tmp_name pre =
    incr tmp_count ;
    sprintf "%s_%d" pre !tmp_count
  in
  object (self)
    method initialize =
      Sys.interactive := true ;
      Sys_js.set_channel_flusher stdout (debug "out: %s") ;
      Sys_js.set_channel_flusher stderr (fun s ->
          error_stack := s :: !error_stack ) ;
      Toploop.set_paths () ;
      ( try JsooTop.initialize () with
      | Typetexp.Error (loc, env, err) as e ->
          debug "Initialization failed with typetexp: %s"
            (Printexc.to_string e) ;
          Typetexp.report_error env Format.std_formatter err ;
          flush stdout
      | e -> debug "Initialization failed: %s" (Printexc.to_string e) ) ;
      error_stack := [] ;
      ()

    method pop_errors =
      let l = !error_stack in
      error_stack := [] ;
      List.rev l

    method eval phrase =
      let buf = Buffer.create 42 in
      let f = Format.formatter_of_buffer buf in
      match
        JsooTop.execute true f (phrase ^ ";;") ;
        Buffer.contents buf
      with
      | "" -> Error (`Eval_failure self#pop_errors)
      | err when String.is_prefix err ~prefix:"Stack overflow during" ->
          Error (`Stack_overflow (err :: self#pop_errors))
      | other -> Ok other
      | exception e -> Error (`Top_level_exception e)

    method get_ocaml_string variable =
      let open Rresult.R.Infix in
      let tmp = tmp_name "get-var" in
      Sys_js.create_file ~name:tmp ~content:"" ;
      let code =
        sprintf
          {ocaml|let x_unit =
                         let o = open_out %S in
                         Printf.fprintf o "%%s\n" %s;
                         close_out o;
                         Printf.printf "Success!!\n%%!"|ocaml}
          tmp variable
      in
      debug "Getting contents of %S in %s with %s" variable tmp code ;
      self#eval code
      >>= fun out ->
      debug "Getting contents of %S: %s" variable out ;
      let s = Sys_js.read_file tmp in
      Ok s
  end

let genspioize code =
  let open Rresult.R.Infix in
  global_ocaml#initialize ;
  global_ocaml#eval "open Genspio.EDSL" |> ignore ;
  let toplevel_eval = ksprintf global_ocaml#eval "let script = %s;;" code in
  let open Protocol.Message.From_worker in
  match toplevel_eval with
  | Error e -> Toplevel_failed e
  | Ok toplevel_success ->
      let try_compmilation phrase =
        match ksprintf global_ocaml#eval "let compiled = %s;;" phrase with
        | Error e -> Error e
        | Ok shell_compilation_output ->
          match global_ocaml#get_ocaml_string "compiled" with
          | Error e -> Error (`Eval_failure [shell_compilation_output])
          | Ok compiled_code -> Ok (shell_compilation_output, compiled_code)
      in
      let stdml_compilation =
        try_compmilation "Genspio.Compile.to_many_lines script"
      in
      let slow_flow_compilation =
        try_compmilation
          "let a = Genspio.Compile.To_slow_flow.compile script in \
           Format.asprintf \"%a\" Genspio.Compile.To_slow_flow.Script.pp_posix a"
      in
      Toplevel {toplevel_success; stdml_compilation; slow_flow_compilation}

let () =
  Worker.set_onmessage (fun msg ->
      debug "WORKER onmessage" ;
      let code = msg |> Protocol.Message.To_worker.of_json in
      debug "WORKER: got %db bytes" (String.length code) ;
      Worker.post_message
        (genspioize code |> Protocol.Message.From_worker.to_json) )
