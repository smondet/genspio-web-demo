open Nonstd
module String = Sosa.Native_string

let debug f = Printf.ksprintf (fun s -> Firebug.console##log (Js.string s)) f

let to_dom e = Tyxml_js.To_dom.of_node e

let blob_of_string (str : string) =
  let varname = sprintf "Blob_of_string_%d" Random.(int 1000_000) in
  Js.export varname
    (object%js
       method code = str
    end) ;
  let blob : File.blob Js.t =
    (* https://github.com/ocsigen/js_of_ocaml/issues/630 *)
    ksprintf Js.Unsafe.eval_string
      "new Blob([%s.code()], {type: \"text/plain\"})" varname
  in
  blob

let worker on_result =
  let wrk = Worker.create "toplevel_worker.js" in
  let is_busy_signal, set_is_busy = React.S.create false in
  let delayed = ref None in
  wrk##.onmessage :=
    Dom_html.handler (fun ev ->
        debug "Back from worker" ;
        set_is_busy false ;
        on_result (Protocol.Message.From_worker.of_json ev##.data) ;
        Js._true) ;
  let count = ref 0 in
  object (self)
    method is_busy = is_busy_signal

    method run s : unit =
      incr count ;
      let run_id = sprintf "run:%d:%d" !count (String.length s) in
      Lwt.async (fun () ->
          let open Lwt in
          let loop () =
            Lwt_js.sleep 1.0
            >>= fun () ->
            match !delayed with
            | None ->
                return ()
            | Some (id, s) ->
                delayed := None ;
                debug "WORKER Woke up trying: %s " id ;
                self#run s ;
                return ()
          in
          loop ()) ;
      match React.S.value is_busy_signal with
      | true ->
          debug "WORKER IS BUSY, NOT SENDING: %s!!!" run_id ;
          delayed := Some (run_id, s)
      | false ->
          debug "WORKER starting %s!!!" run_id ;
          set_is_busy true ;
          wrk##postMessage (Protocol.Message.To_worker.to_json s)
  end

module WMsg = Protocol.Message.From_worker

let gui () =
  debug "Go! Genspio: %s" Metadata.genspio_version ;
  let result_signal, set_result = React.S.create WMsg.Not_ready in
  let wrk = worker set_result in
  debug "arguments: [%s]"
    ( List.map ~f:(fun (x, y) -> sprintf "(%S, %S)" x y) Url.Current.arguments
    |> String.concat ~sep:"; " ) ;
  let initial_code =
    List.Assoc.get "input" Url.Current.arguments
    |> Option.value_map ~f:Url.urldecode
         ~default:
           "seq [\n\
            call [string \"echo\"; string \"Hello World\"] ||> exec [\"cat\"];\n\
            ]\n"
  in
  wrk#run initial_code ;
  let code_signal, set_code = React.S.create initial_code in
  let download_string_link ?filename str content =
    let blob = blob_of_string str in
    let url = Dom_html.window##._URL##createObjectURL blob in
    let open Tyxml_js.Html in
    a ~a:[a_href (Js.to_string url); a_download filename] content
  in
  let share_url code =
    let url = Url.Current.get () |> Option.value_exn ~msg:"NO URL!!" in
    let new_arg = ("input", Url.urlencode code) in
    let change_arg l =
      new_arg
      :: List.filter l ~f:(fun (arg, _) -> arg <> "input" && arg <> "?input")
    in
    let open Url in
    ( match url with
    | Https u ->
        Https {u with hu_arguments= change_arg u.hu_arguments}
    | Http u ->
        Http {u with hu_arguments= change_arg u.hu_arguments}
    | File u ->
        File {u with fu_arguments= change_arg u.fu_arguments} )
    |> string_of_url
  in
  let result_choice_signal, set_result_choice = React.S.create `Typing in
  let open Tyxml_js.Html in
  let code_mirrored = ref false in
  let ensure_code_mirrored () =
    if not !code_mirrored then
      let (_ : unit) =
        Js.Unsafe.eval_string
          {js|
var editor = CodeMirror.fromTextArea(document.getElementById("editor-text-area"),
{
mode: "mllike",
onChange: function(cm){
   console.log("Cm save" + cm);
   cm.save();
   document.getElementById("editor-text-area").change()
},
lineWrapping: true,
lineNumbers: true
});
window.cm = editor;
editor.on('change', editor => {
    editor.save();
    var evt = document.createEvent("HTMLEvents");
    evt.initEvent("change", false, true);
    document.getElementById("editor-text-area")
       .dispatchEvent(evt);
});
/* See https://codemirror.net/demo/indentwrap.html */
var charWidth = editor.defaultCharWidth(), basePadding = 4;
editor.on("renderLine", function(cm, line, elt) {
    var off = (2 + CodeMirror.countColumn(line.text, null, cm.getOption("tabSize"))) * charWidth;
    elt.style.textIndent = "-" + off + "px";
        elt.style.paddingLeft = (basePadding + off) + "px";
    });
editor.refresh();
|js}
      in
      code_mirrored := true
  in
  let editor_command_button ~text command =
    button [pcdata text]
      ~a:
        [ a_class ["btn"; "btn-secondary"]
        ; a_onclick (fun _ ->
              let _ =
                Js.Unsafe.meth_call
                  (Js.Unsafe.get Dom_html.window (Js.string "cm"))
                  "execCommand"
                  [|Js.string command |> Js.Unsafe.inject|]
              in
              let _ =
                Js.Unsafe.meth_call
                  (Js.Unsafe.get Dom_html.window (Js.string "cm"))
                  "focus" [||]
              in
              true) ]
  in
  div
    ~a:[a_class ["container"]]
    [ div
        ~a:[a_class ["jumbotron"]]
        [ h1 [pcdata "Genspio@"; code [pcdata Metadata.genspio_version]]
        ; p
            [ pcdata "Welcome to the "
            ; a
                ~a:[a_href "https://github.com/hammerlab/genspio"]
                [pcdata "Genspio"]
            ; pcdata " playground." ] ]
    ; div
        ~a:[a_class ["col-md-6"]]
        [ p
            ~a:[a_class ["lead"]]
            [ pcdata "Input your Genspio expression ("
            ; pcdata "shareable "
            ; a
                ~a:[Tyxml_js.R.Html.a_href (React.S.map share_url code_signal)]
                [pcdata "link"]
            ; pcdata "):" ]
        ; editor_command_button ~text:"Undo" "undo"
        ; editor_command_button ~text:"Redo" "redo"
        ; editor_command_button ~text:"Select All" "selectAll"
        ; textarea
            ~a:
              [ a_id "editor-text-area"
              ; a_class ["form-control"]
              ; a_style "font-family: monospace"
              ; a_rows 24
              ; a_onchange (fun ev ->
                    Js.Opt.iter ev##.target (fun input ->
                        Js.Opt.iter (Dom_html.CoerceTo.textarea input)
                          (fun input ->
                            let v = input##.value |> Js.to_string in
                            debug "TA inputs: %d bytes" (String.length v) ;
                            set_code v ;
                            wrk#run v)) ;
                    false) ]
            (pcdata initial_code) ]
    ; div
        ~a:[a_class ["col-md-6"]]
        [ div
            [ strong [pcdata "Intepreter status: "]
            ; Tyxml_js.R.Html.span
                (let disp =
                   let badge cl labl =
                     [span ~a:[a_class ["label"; "label-" ^ cl]] [pcdata labl]]
                   in
                   function
                   | true ->
                       badge "warning" "WORKING …"
                   | false ->
                       badge "success" "READY"
                 in
                 React.S.map disp wrk#is_busy |> ReactiveData.RList.from_signal)
            ]
        ; hr ()
        ; Tyxml_js.R.Html.div
            (let disp_result result choice =
               let typing_success comp =
                 div
                   ~a:[a_class ["alert"; "alert-dismissible"; "alert-success"]]
                   [ strong [pcdata "Type-checking succeed!"]
                   ; br ()
                   ; pre [code [pcdata comp]] ]
               in
               let disp_error ?(extra_message = span []) kind eval_error =
                 let codebloc c = pre [code c] in
                 div
                   ~a:[a_class ["alert"; "alert-dismissible"; "alert-danger"]]
                   [ strong [ksprintf pcdata "%s error:" kind]
                   ; br ()
                   ; extra_message
                   ; br ()
                   ; ( match eval_error with
                     | `Stack_overflow sl ->
                         div
                           [ pcdata
                               "The evaluation (parsing/type-checking) \
                                crashed with a stack-overflow:"
                           ; codebloc
                             @@ List.map sl ~f:(fun s ->
                                    pcdata (sprintf "%s" s)) ]
                     | `Eval_failure sl ->
                         div
                           [ pcdata "The evaluation failed:"
                           ; codebloc
                             @@ List.map sl ~f:(fun s ->
                                    pcdata (sprintf "%s" s)) ]
                     | `Top_level_exception e ->
                         div
                           [ pcdata "The evaluator itself crashed:"
                           ; codebloc [pcdata @@ Printexc.to_string e] ] ) ]
               in
               let disp_script_result res =
                 let mkdiv as_error content =
                   let color =
                     if as_error then "alert-danger" else "alert-success"
                   in
                   div
                     ~a:[a_class ["alert"; "alert-dismissible"; color]]
                     [ strong
                         [ ksprintf pcdata "Compilation to POSIX %s:"
                             (if not as_error then "succeeded" else "failed")
                         ]
                     ; br ()
                     ; ( if as_error then span []
                       else
                         download_string_link content ~filename:"genspio.sh"
                           [pcdata "Download script"] )
                     ; pre [code [pcdata content]] ]
                 in
                 match res with
                 | Ok (_evaluation, script) ->
                     mkdiv false script
                 | Error err ->
                     let version_message =
                       match Metadata.genspio_version with
                       | "genspio.0.0.0" | "genspio.0.0.1" ->
                           strong
                             [ pcdata
                                 "This version of Genspio does not yet \
                                  support the"
                             ; code [pcdata "Slow_flow"]
                             ; pcdata "compiler." ]
                       | _ ->
                           span []
                     in
                     disp_error ~extra_message:version_message "Compilation"
                       err
               in
               let choice_buttons choices choice =
                 let mkbtn which choice status =
                   let classes =
                     let color =
                       match status with
                       | `Ok ->
                           "btn-success"
                       | `Error ->
                           "btn-danger"
                     in
                     match which = choice with
                     | true ->
                         ["btn"; color]
                     | false ->
                         ["btn"; color; "active"]
                   in
                   let lbl =
                     match which with
                     | `Typing ->
                         "Type-checking"
                     | `StdML ->
                         "Main POSIX Compiler"
                     | `Slow_flow ->
                         "Slow-flow POSIX Compiler"
                   in
                   let a =
                     match which <> choice with
                     | true ->
                         [ a_class classes
                         ; a_onclick (fun _ -> set_result_choice which ; true)
                         ]
                     | false ->
                         [a_class classes]
                   in
                   label [pcdata lbl] ~a
                 in
                 div
                   ~a:[a_class ["btn-group"]]
                   (List.map choices ~f:(fun (which, status) ->
                        mkbtn which choice status))
               in
               match result with
               | WMsg.Not_ready ->
                   [i [pcdata "NOT READY ... YET"]]
               | WMsg.Toplevel_failed err ->
                   ensure_code_mirrored () ;
                   [disp_error "Evaluation" err]
               | WMsg.Toplevel
                   { WMsg.toplevel_success
                   ; stdml_compilation
                   ; slow_flow_compilation } ->
                   ensure_code_mirrored () ;
                   let btnstatus = function
                     | Ok _ ->
                         `Ok
                     | Error _ ->
                         `Error
                   in
                   [ choice_buttons
                       [ (`Typing, `Ok)
                       ; (`StdML, btnstatus stdml_compilation)
                       ; (`Slow_flow, btnstatus slow_flow_compilation) ]
                       choice
                   ; ( match choice with
                     | `Typing ->
                         typing_success toplevel_success
                     | `StdML ->
                         disp_script_result stdml_compilation
                     | `Slow_flow ->
                         disp_script_result slow_flow_compilation ) ]
             in
             React.S.l2 disp_result result_signal result_choice_signal
             |> ReactiveData.RList.from_signal) ] ]

let attach_to_page gui =
  let base_div = Dom_html.getElementById "here" in
  base_div##appendChild (to_dom gui) |> ignore ;
  Lwt.return ()

let go _ =
  debug "Hello Go!" ;
  ignore
    Lwt.(
      catch
        (fun () -> attach_to_page (gui ()) >>= fun () -> return ())
        (fun exn ->
          Printf.ksprintf
            (fun s ->
              Firebug.console##error (Js.string s) ;
              failwith s)
            "Uncaught Exception: %s" (Printexc.to_string exn))) ;
  Js._true

let _ =
  debug "Hello Main!" ;
  Dom_html.window##.onload := Dom_html.handler go
