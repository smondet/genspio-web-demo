Genspio Web Tests
=================

Build:

    opam install --yes rresult js_of_ocaml-tyxml.3.0.1 js_of_ocaml-toplevel.3.0.1 js_of_ocaml-lwt.3.0.1 nonstd ocaml-migrate-parsetree rresult sosa genspio.0.0.2 base
    sh build.sh

Then visit `_build/custom/index.html` with a web-server, for instance with:

    ( cd _build/custom/ ; python -m SimpleHTTPServer 8080 )

And go to <http://localhost:8080/>.

<div style="padding: 2em; margin: 2em;"><a href="https://user-images.githubusercontent.com/617111/33030073-56815ae2-cde8-11e7-8406-c3986e682973.gif"><img
 width="80%"
  src="https://user-images.githubusercontent.com/617111/33030073-56815ae2-cde8-11e7-8406-c3986e682973.gif"
></a></div>

**Note on dependencies:** in order to build against various versions of Genspio,
this demo ships the libraries `sosa`, `nonstd`, *and* `base`.
